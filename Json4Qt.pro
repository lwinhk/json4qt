# -------------------------------------------------
# Project created by QtCreator 2013-08-12T15:46:34
# -------------------------------------------------
QT -= gui
TARGET = Json4Qt
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += main.cpp \
    json.cpp
HEADERS += json.h \
    json_global.h
