#include <QtCore/QCoreApplication>
#include <iostream>
#include <QDebug>
#include <QFile>
#include "json.h"

using namespace Json;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

   // QString str = "{\"key\":{\"sub_key\":\"sub_value\"},\"key2\":\"value2\"}";
    QString str = "{{\"A\"},{\"B\"},{\"C\"}}";
    QString str2 = "{\"key\":[\"A\",\"B\", \"C\"],\"key2\":\"value2\"}";
    QString str3;

    QFile file("job_offers.txt");
    if(file.open(QIODevice::ReadOnly)) {
        str3 = file.readAll();
        file.close();
    }

    Json::JsonObject *obj = new Json::JsonObject();
    obj->parse(str);

    qDebug() << obj->size();

    qDebug() << "--------------------";
    if(obj->size() > 0) {
        if(obj->get_object(0)->member_type() == JSON_PAIR) {
            JsonPair* pair = (JsonPair*) obj->get_object(0);
            if(pair) {
                qDebug() << pair->key();
                qDebug() << pair->value();
            }
            else {
                qDebug() << "object is null";
            }
        }
        else {
            JsonElement *element = (JsonElement*) obj->get_object(0);
            if(element) {
                //qDebug() << element->value();
            }
        }
    }

    for(int i = 0; i < obj->size(); i++) {
        if(obj->get_object(i)->member_type() == JSON_PAIR) {
            JsonPair * pair = (JsonPair*) obj->get_object(i);
            if(pair) {
                qDebug() << pair->to_string();
            }
            else {
                qDebug() << "empty";
            }
        }
        else if(obj->get_object(i)->member_type() == JSON_ELEMENT) {
            JsonElement * element = (JsonElement*) obj->get_object(i);
            if(element) {
                qDebug() << element->to_string();
            }
            else {
                qDebug() << "empty";
            }
        }
    }

    /*
    JsonObject *child_hash = new JsonObject(obj->get("main_slots"));
    qDebug() << child_hash->get("key");
    JsonObject* k = new JsonObject(child_hash->get("key"));
    qDebug() << k->get("sub_key");

    qDebug() << child_hash->get("key2");
    //for(int i = 0; i < obj->size(); i++) {
    //    std::cout << obj->get(i).toAscii().data() << std::endl;
    //}
    */

    qDebug() << "--------------------";

    JsonObject *obj2 = new JsonObject();
    obj2->parse(str2);

    qDebug() << obj2->size();

    for(int i = 0; i < obj2->size(); i++) {
        qDebug() << obj2->get(i);
    }

    JsonObject *obj3 = new JsonObject(obj2->get(0));
    qDebug() << obj3->size();

    for(int i = 0; i < obj3->size(); i++) {
        qDebug() << obj3->get(i);
    }
    qDebug() << obj3->to_string();


    JsonObject* jb = new JsonObject(str3);
    qDebug() << jb->size();
    for(int i = 0; i < jb->size(); i++) {
        qDebug() << jb->get(i) << "\n";
        JsonObject * sub_obj = new JsonObject(jb->get(i));
        for(int j = 0; j < sub_obj->size(); j++) {
            qDebug() << sub_obj->get(j) << "\n";
            JsonObject * sso = new JsonObject(sub_obj->get(j));
            if(sso) {
                for(int k = 0; k < sso->size(); k++) {
                    qDebug() << sso->get(k);
                }
            }
        }
    }

    return a.exec();
}
