/**
    Library     - Json Parser for Qt C++. Unicode may not be supported. Please try.
    Developer   - Lwin Htoo Ko (lwinhk2008@gmail.com)
    License     - GNU Lesser General Public License
**/

#include "json.h"
#include <QDebug>

using namespace Json;

JsonMember::JsonMember() {
}

MEMBER_TYPE JsonMember::member_type() {
    return mem_type;
}

JsonPair::JsonPair() {
    mem_type = JSON_PAIR;
    e_type = JSON_STRING;
}

JsonPair::JsonPair(QString key, QString value) {
    mem_type = JSON_PAIR;
    e_type = JSON_STRING;

    set_key(key);
    set_value(value);
}

QString JsonPair::key() {
    return json_key;
}

QString JsonPair::value() {
    return json_value;
}

QString JsonPair::to_string() {
    QString ret_string;
    ret_string.append(key());
    ret_string.append(JSON_PAIR_INDICATOR);
    if(e_type == JSON_STRING)
        ret_string.append("\"" + value() + "\"");
    else if(e_type == JSON_NULL)
        return "";
    else
        ret_string.append(value());

    return ret_string;
}

void JsonPair::set_key(QString key) {
    json_key = key;
}

void JsonPair::set_value(QString value) {
    json_value = value;
}

ELEMENT_TYPE JsonPair::element_type() {
    return e_type;
}

JsonElement::JsonElement() {
    mem_type = JSON_ELEMENT;
    e_type = JSON_STRING;
}

JsonElement::JsonElement(QString value) {
    mem_type = JSON_ELEMENT;
    e_type = JSON_STRING;

    set_value(value);
}

void JsonElement::set_value(QString value) {
    json_value = value;
}

QString JsonElement::value() {
    return json_value;
}

QString JsonElement::to_string() {
    if(e_type == JSON_STRING)
        return "\"" + value() + "\"";
    else if(e_type == JSON_NULL)
        return "";
    else
        return value();
}

ELEMENT_TYPE JsonElement::element_type() {
    return e_type;
}

JsonObject::JsonObject()
{
}

JsonObject::JsonObject(QString json_string) {
    parse(json_string);
}

void JsonObject::parse(QString json_string) {
    if(json_string == "" || json_string.size() < 1) {
        return;
    }
    QString raw_string = json_string.trimmed();
    data = raw_string;

    if(raw_string.at(0) == JSON_OBJECT_START) {
        //object_type = JSON_OBJECT;

        JsonPair *pair = 0;
        //JsonElement *object = 0;
        QString token = "";
        bool is_pair = false;
        bool found_sub_arr = false;
        bool found_sub_obj = false;
        int start_count = 0;
        int end_count = 0;
        // 0 = nothing, 1 = at_string_start, 2 = at_string_end, 3 = processing, 4 = not_string
        int condition = 0;
        for(int i = 1; i < raw_string.size(); i++) {

            //qDebug() << "current_tokn = " << token;
            //qDebug() << "current_char = " << raw_string.at(i);
            //qDebug() << "members size = " << members.size();

            if(raw_string.at(i) == JSON_STRING_START) {
                // get string start/end (")
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(condition == 0) {
                        // if condition is 0, it is first time getting string start/end (").
                        // start recording token.
                        condition = 1;
                    }
                    else if(condition == 1) {
                        // getting two consecutive string start/end ""
                        // save token as empty
                        condition = 2;
                    }
                    else if(condition == 3) {
                        // if condition is 3, processing finishes and token ends.
                        condition = 2;
                    }
                    else {
                        // if condition 2, it is getting three consecutive string start/end """
                        has_err = true;
                        err_pos = i;
                        err_msg = ERROR_INVALID_STRING;
                        return;
                    }
                }
            }
            else if(raw_string.at(i) == JSON_PAIR_INDICATOR) {
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else if(condition == 3) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(condition == 0) {
                        has_err = true;
                        err_pos = i;
                        err_msg = ERROR_INVALID_PAIR_INDICATOR_POSITION;
                        return;
                    }
                    is_pair = true;
                    pair = new JsonPair();
                    //members.append(pair);
                    pair->set_key(token);
                    token.clear();

                    condition = 0;
                }
            }
            else if(raw_string.at(i) == JSON_SEPARATOR) {
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else if(condition == 3) {
                    token.append(raw_string.at(i));
                }
                else {
                    /*
                    if(condition == 0) {
                        has_err = true;
                        err_pos = i;
                        err_msg = ERROR_INVALID_SEPARATOR_POSITION;
                        return 0;
                    }*/
                    if(is_pair) {
                        if(pair) {
                            pair->set_value(token);
                            members.append(pair);
                            token.clear();
                        }
                    }
                    else {
                        JsonElement *obj = new JsonElement(token);
                        members.append(obj);
                        token.clear();
                    }
                    is_pair = false;

                    condition = 0;
                }
            }
            else if(raw_string.at(i) == JSON_OBJECT_START && i != (raw_string.size() - 1)) {
                if(!found_sub_arr) {
                    token.append(raw_string.at(i));
                    start_count++;
                    //qDebug() << "sc = " << start_count << ", ec = " << end_count;
                    if(!found_sub_obj)
                        found_sub_obj = true;
                }
                else {
                    token.append(raw_string.at(i));
                }
            }
            else if(raw_string.at(i) == JSON_OBJECT_END && i != (raw_string.size() - 1)) {
                if(!found_sub_arr) {
                    token.append(raw_string.at(i));
                    end_count++;
                    //qDebug() << "sc = " << start_count << ", ec = " << end_count;
                    if(start_count == end_count) {
                        if(is_pair) {
                            if(pair) {
                                //pair->set_value(token);
                                //token.clear();
                            }
                        }
                        else {
                            //JsonElement *obj = new JsonElement(token);
                            //members.append(obj);
                            //token.clear();
                        }
                        //is_pair = false;

                        condition = 0;
                        start_count = 0;
                        end_count = 0;
                        if(found_sub_obj)
                            found_sub_obj = false;
                    }
                }
                else {
                    token.append(raw_string.at(i));
                }
            }
            else if(raw_string.at(i) == JSON_ARRAY_START && i != (raw_string.size() - 1)) {
                if(!found_sub_obj) {
                    token.append(raw_string.at(i));
                    start_count++;
                    //qDebug() << "sc = " << start_count << ", ec = " << end_count;
                    if(!found_sub_arr)
                        found_sub_arr = true;
                }
                else {
                    token.append(raw_string.at(i));
                }
            }
            else if(raw_string.at(i) == JSON_ARRAY_END && i != (raw_string.size() - 1)) {
                if(!found_sub_obj) {
                    token.append(raw_string.at(i));
                    end_count++;
                    //qDebug() << "sc = " << start_count << ", ec = " << end_count;
                    if(start_count == end_count) {
                        if(is_pair) {
                            if(pair) {
                                //pair->set_value(token);
                                //token.clear();
                            }
                        }
                        else {
                            JsonElement *obj = new JsonElement(token);
                            members.append(obj);
                            token.clear();
                        }
                        //is_pair = false;

                        condition = 0;
                        start_count = 0;
                        end_count = 0;
                        if(found_sub_arr)
                            found_sub_arr = false;
                    }
                }
                else {
                    token.append(raw_string.at(i));
                }
            }
            else if(raw_string.at(i) == JSON_OBJECT_END && i == (raw_string.size() - 1)) {
                // end of raw_string
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(is_pair) {
                        if(pair) {
                            pair->set_value(token);
                            members.append(pair);
                            token.clear();
                        }
                    }
                    else {
                        JsonElement *obj = new JsonElement(token);
                        members.append(obj);
                        token.clear();
                    }
                    is_pair = false;

                    condition = 0;
                }
            }
            else if(raw_string.at(i) == JSON_WHITE_SPACE) {
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(condition == 1) {
                        condition = 3;
                        token.append(raw_string.at(i));
                    }
                    else if(condition == 3) {
                        token.append(raw_string.at(i));
                    }
                }
            }
            else {
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(condition == 0) {
                        condition = 4;
                        token.append(raw_string.at(i));
                    }
                    else if(condition == 1) {
                        condition = 3;
                        token.append(raw_string.at(i));
                    }
                    else if(condition == 3) {
                        token.append(raw_string.at(i));
                    }
                    else if(condition == 4) {
                        token.append(raw_string.at(i));
                    }
                }
            }
        }
    }
    else if(raw_string.at(0) == JSON_STRING_START) {
        //object_type = JSON_STRING;
    }
    else if(raw_string.at(0) == JSON_ARRAY_START) {
        // " or {} or others

        QString token = "";
        bool found_sub_arr = false;
        bool found_sub_obj = false;
        int start_count = 0;
        int end_count = 0;
        // 0 = nothing, 1 = at_string_start, 2 = at_string_end, 3 = processing, 4 = not_string
        int condition = 0;
        for(int i = 1; i < raw_string.size(); i++) {

            //qDebug() << "current_tokn = " << token;
            //qDebug() << "current_char = " << raw_string.at(i);
            //qDebug() << "members size = " << members.size();

            if(raw_string.at(i) == JSON_STRING_START) {
                // get string start/end (")
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(condition == 0) {
                        // if condition is 0, it is first time getting string start/end (").
                        // start recording token.
                        condition = 1;
                    }
                    else if(condition == 1) {
                        // getting two consecutive string start/end ""
                        // save token as empty
                        condition = 2;
                    }
                    else if(condition == 3) {
                        // if condition is 3, processing finishes and token ends.
                        condition = 2;
                    }
                    else {
                        // if condition 2, it is getting three consecutive string start/end """
                        has_err = true;
                        err_pos = i;
                        err_msg = ERROR_INVALID_STRING;
                        return;
                    }
                }
            }
            else if(raw_string.at(i) == JSON_SEPARATOR) {
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else if(condition == 3) {
                    token.append(raw_string.at(i));
                }
                else {
                    JsonElement *obj = new JsonElement(token);
                    members.append(obj);
                    token.clear();

                    condition = 0;
                }
            }
            else if(raw_string.at(i) == JSON_OBJECT_START && i != (raw_string.size() - 1)) {
                if(!found_sub_arr) {
                    token.append(raw_string.at(i));
                    start_count++;
                    //qDebug() << "sc = " << start_count << ", ec = " << end_count;
                    if(!found_sub_obj)
                        found_sub_obj = true;
                }
                else {
                    token.append(raw_string.at(i));
                }
            }
            else if(raw_string.at(i) == JSON_OBJECT_END && i != (raw_string.size() - 1)) {
                if(!found_sub_arr) {
                    token.append(raw_string.at(i));
                    end_count++;
                    //qDebug() << "sc = " << start_count << ", ec = " << end_count;
                    if(start_count == end_count) {
                        //JsonElement *obj = new JsonElement(token);
                        //members.append(obj);
                        //token.clear();

                        condition = 0;
                        start_count = 0;
                        end_count = 0;
                        if(found_sub_obj)
                            found_sub_obj = false;
                    }
                }
                else {
                    token.append(raw_string.at(i));
                }
            }
            else if(raw_string.at(i) == JSON_ARRAY_START && i != (raw_string.size() - 1)) {
                if(!found_sub_obj) {
                    token.append(raw_string.at(i));
                    start_count++;
                    //qDebug() << "sc = " << start_count << ", ec = " << end_count;
                    if(!found_sub_arr)
                        found_sub_arr = true;
                }
                else {
                    token.append(raw_string.at(i));
                }
            }
            else if(raw_string.at(i) == JSON_ARRAY_END && i != (raw_string.size() - 1)) {
                if(!found_sub_obj) {
                    token.append(raw_string.at(i));
                    end_count++;
                    //qDebug() << "sc = " << start_count << ", ec = " << end_count;
                    if(start_count == end_count) {
                        JsonElement *obj = new JsonElement(token);
                        members.append(obj);
                        token.clear();

                        condition = 0;
                        start_count = 0;
                        end_count = 0;
                        if(found_sub_arr)
                            found_sub_arr = false;
                    }
                }
                else {
                    token.append(raw_string.at(i));
                }
            }
            else if(raw_string.at(i) == JSON_ARRAY_END && i == (raw_string.size() - 1)) {
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(token.size() > 0) {
                        JsonElement *obj = new JsonElement(token);
                        members.append(obj);
                    }

                    token.clear();

                    condition = 0;
                }
            }
            else if(raw_string.at(i) == JSON_OBJECT_END && i == (raw_string.size() - 1)) {
                // end of raw_string
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    JsonElement *obj = new JsonElement(token);
                    members.append(obj);
                    token.clear();

                    condition = 0;
                }
            }
            else if(raw_string.at(i) == JSON_WHITE_SPACE) {
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(condition == 1) {
                        condition = 3;
                        token.append(raw_string.at(i));
                    }
                    else if(condition == 3) {
                        token.append(raw_string.at(i));
                    }
                }
            }
            else {
                if(found_sub_arr || found_sub_obj) {
                    token.append(raw_string.at(i));
                }
                else {
                    if(condition == 0) {
                        condition = 4;
                        token.append(raw_string.at(i));
                    }
                    else if(condition == 1) {
                        condition = 3;
                        token.append(raw_string.at(i));
                    }
                    else if(condition == 3) {
                        token.append(raw_string.at(i));
                    }
                    else if(condition == 4) {
                        token.append(raw_string.at(i));
                    }
                }
            }
        }


    }
    else {
        // integer, float, boolean
        //object_type = JSON_OTHERS;

        QString token;
        for(int i = 0; i < raw_string.size(); i++) {
            token = raw_string;
            data = raw_string;
        }
    }
}

int JsonObject::size() {
    return members.size();
}

QString JsonObject::get(int index) {
    if(index < members.size() && index > -1) {
        if(members.at(index)->member_type() == JSON_PAIR) {
            JsonPair* pair = (JsonPair*) members.at(index);
            return pair->value();
        }
        else {
            JsonElement* element = (JsonElement*) members.at(index);
            return element->value();
        }
    }
    return "";
}

QString JsonObject::get(QString key) {
    for(int i = 0; i < members.size(); i++) {
        if(members.at(i)->member_type() == JSON_PAIR) {
            JsonPair* pair = (JsonPair*) members.at(i);
            if(pair->key() == key) {
                return pair->value();
            }
        }
    }
    return "";
}

JsonMember* JsonObject::get_object(int index) {
    if(index < members.size() && index > -1) {
        return members.at(index);
    }
    return 0;
}

void JsonObject::set_object(JsonMember *member) {
    members.append(member);
}

QString JsonObject::to_string() {
    QString ret_string;

    for(int i = 0; i < members.size(); i++) {
        if(i != 0) {
            ret_string.append(JSON_SEPARATOR);
            if(members.at(i)->member_type() == JSON_PAIR) {
                JsonPair* pair = (JsonPair*) members.at(i);
                ret_string.append(pair->to_string());
            }
            else
            {
                JsonElement* obj = (JsonElement*) members.at(i);
                ret_string.append(obj->to_string());
            }
        }
        else {
            if(members.at(i)->member_type() == JSON_PAIR) {
                JsonPair* pair = (JsonPair*) members.at(i);
                ret_string.append(pair->to_string());
            }
            else
            {
                JsonElement* obj = (JsonElement*) members.at(i);
                ret_string.append(obj->to_string());
            }
        }
    }
    return ret_string;
}
