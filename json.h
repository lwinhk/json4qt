/**
    Library     - Json Parser for Qt C++. Unicode may not be supported. Please try.
    Developer   - Lwin Htoo Ko (lwinhk2008@gmail.com)
    License     - GNU Lesser General Public License
**/

#ifndef JSON_H
#define JSON_H

#include <QString>
#include <QList>
#include "json_global.h"

namespace Json {

    class JsonMember {
    public:
        JsonMember();
        MEMBER_TYPE member_type();

    protected:
        MEMBER_TYPE mem_type;
    };

    class JsonPair : public JsonMember {
    public:
        JsonPair();
        JsonPair(QString key, QString value);

        void set_key(QString key);
        void set_value(QString value);
        QString key();
        QString value();
        QString to_string();
        ELEMENT_TYPE element_type();

    private:
        QString json_key;
        QString json_value;
        ELEMENT_TYPE e_type;
    };

    class JsonElement : public JsonMember {
    public:
        JsonElement();
        JsonElement(QString value);

        void set_value(QString value);
        QString value();
        QString to_string();
        ELEMENT_TYPE element_type();
    private:
        QString json_value;
        ELEMENT_TYPE e_type;
    };

    class JsonObject {
    public:
        JsonObject();
        JsonObject(QString json_string);

        void parse(QString json_string);
        int size();
        QString get(int index);
        QString get(QString key);
        JsonMember* get_object(int index);
        void set_object(JsonMember *member);
        QString to_string();

    private:
        QString data;
        QList<JsonMember*> members;

        bool has_err;
        int err_pos;
        QString err_msg;
    };
}

#endif // JSON_H
