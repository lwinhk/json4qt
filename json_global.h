/**
    Library     - Json Parser for Qt C++. Unicode may not be supported. Please try.
    Developer   - Lwin Htoo Ko (lwinhk2008@gmail.com)
    License     - GNU Lesser General Public License
**/

#ifndef JSON_GLOBAL_H
#define JSON_GLOBAL_H

namespace Json {
    enum MEMBER_TYPE {
        JSON_PAIR = 1,
        JSON_ELEMENT = 2
    };

    enum ELEMENT_TYPE {
        JSON_STRING = 1,
        JSON_NOT_STRING = 2,
        JSON_NULL = 3
    };

    enum JSON_TYPE {
        JSON_OBJECT = 1,
        //JSON_STRING = 2,
        JSON_ARRAY = 3,
        JSOB_PAIR = 4,
        JSON_OTHERS = 5
    };

#define JSON_SEPARATOR ','
#define JSON_PAIR_INDICATOR ':'
#define JSON_STRING_START '"'
#define JSON_STRING_END '"'
#define JSON_OBJECT_START '{'
#define JSON_OBJECT_END '}'
#define JSON_ARRAY_START '['
#define JSON_ARRAY_END ']'
#define JSON_WHITE_SPACE ' '

#define ERROR_INVALID_STRING "Invalid string found"
#define ERROR_INVALID_PAIR_INDICATOR_POSITION "Invalid pair indicator position"
#define ERROR_INVALID_SEPARATOR_POSITION "Invalid separator position"
}

#endif // JSON_GLOBAL_H
